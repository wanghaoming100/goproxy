package main

import "time"
import "fmt"
import "strconv"

var chanMap = make(map[string]chan bool)

func main() {
	c := make(chan int, 1)
	for index := 0; index < 1; index++ {
		go send(c)
	}

	for index := 0; index < 12; index++ {
		go rec(c)
	}
	time.Sleep(5 * time.Second)
	if chanMap["aaa"] == nil {
		fmt.Print("aaa")
	}
}

func rec(c chan int) {
	fmt.Println("recieve mes " + strconv.Itoa(<-c))
	fmt.Println("ha")
}

func send(c chan int) {
	c <- 1
	time.Sleep(3 * time.Second)
	close(c)
}
