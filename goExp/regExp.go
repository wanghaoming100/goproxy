package main

import (
	"fmt"
	"regexp"
)

func main() {
	// 查找 Hello 或 Go，替换为 Hellooo、Gooo
	text := `Hello 世界！123 Go.`
	reg := regexp.MustCompile(`\d+`)
	fmt.Printf("%q\n", reg.ReplaceAllString(text, "${n}"+"3.2"))
	// "Hellooo 世界！123 Gooo."
}
