package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/patrickmn/go-cache"
)

var fileCache = cache.New(30*time.Second, 1*time.Second)
var chanMap = make(map[string]chan bool)
var reqCount = make(map[string]int)

func init() {
	log.SetFlags(log.Lmicroseconds)
}
func requestHandler(w http.ResponseWriter, r *http.Request) {
	path := r.URL.Path
	url := "http://localhost:1935" + path 
	if strings.HasSuffix(url, ".ts") {
		reqCount[url]++
	}
	log.Println("reqCount is " + strconv.Itoa(reqCount[url]))
	log.Println(url)
	handRes(url, w)
}

func handRes(url string, w http.ResponseWriter) {
	file, found := fileCache.Get(url)
	if found {
		if strings.HasSuffix(url, ".m3u8") {
			responseM3u8(file.(string), w)
		} else {
			// time.Sleep(300 * time.Millisecond)
			responseTs(file.([]byte), w)
		}
	} else {
		if chanMap[url] == nil {
			goResquest := make(chan bool, 1)
			chanMap[url] = goResquest
			go httpGet(url, goResquest)
			log.Println(<-goResquest)
		} else {
			goResquest := chanMap[url]
			log.Println(<-goResquest)
		}
		file, found := fileCache.Get(url)
		if found {
			if strings.HasSuffix(url, ".m3u8") {
				responseM3u8(file.(string), w)
			} else {
				time.Sleep(3000 * time.Millisecond)
				responseTs(file.([]byte), w)
			}
		}
	}

}

func responseM3u8(body string, w http.ResponseWriter) {
	w.Header().Set("Access-Control-Expose-Headers", "Date, Server, Content-Type, Content-Length")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	w.Header().Set("Access-Control-Allow-Methods", "OPTIONS, GET, POST, HEAD")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type, User-Agent, If-Modified-Since, Cache-Control, Range")
	w.Header().Set("Content-Type", "application/vnd.apple.mpegurl")
	log.Println(body)
	fmt.Fprintf(w, body)
}

func responseTs(body []byte, w http.ResponseWriter) {
	w.Header().Set("Access-Control-Expose-Headers", "Date, Server, Content-Type, Content-Length")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	w.Header().Set("Access-Control-Allow-Methods", "OPTIONS, GET, POST, HEAD")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type, User-Agent, If-Modified-Since, Cache-Control, Range")
	w.Header().Set("Content-Type", "application/vnd.apple.mpegurl")
	buf := new(bytes.Buffer)
	buf.Write(body)
	w.Header().Set("Content-Length", strconv.Itoa(buf.Len()))
	buf.WriteTo(w)
}

func httpGet(url string, done chan bool) {
	log.Println("try to get of url " + url)
	for index := 0; index < 20; index++ {
		resp, err := http.Get(url)
		if err != nil {
			log.Println(err)
			time.Sleep(200 * time.Millisecond)
			continue
		}
		defer resp.Body.Close()
		body, err := ioutil.ReadAll(resp.Body)
		if resp.StatusCode > 400 || err != nil {
			time.Sleep(200 * time.Millisecond)
		} else {
			if strings.HasSuffix(url, "playlist.m3u8") {
				m3u8 := string(body)
				fileCache.Set(url, m3u8, 24*time.Hour)
			} else if strings.HasSuffix(url, "chunklist.m3u8") {
				m3u8 := string(body)
				// m3u8 = patchM3u8(m3u8)
				m3u8 = fixTargetDuration(m3u8)
				fileCache.Set(url, m3u8, 1*time.Second)
			} else {
				fileCache.Set(url, body, cache.DefaultExpiration)
			}
			break
		}

	}

	delete(chanMap, url)
	done <- true
	close(done)

}
func main() {
	http.HandleFunc("/", requestHandler)
	err := http.ListenAndServe(":3000", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

func patchM3u8(m3u8 string) string {
	m3u8List := strings.Split(m3u8, "\n")
	infTag := m3u8List[len(m3u8List)-3]
	playlistTag := m3u8List[len(m3u8List)-2]

	ss := strings.Split(playlistTag, "_")
	tsPrefix := strings.Replace(playlistTag, ss[len(ss)-1], "", 1)
	no, error := strconv.Atoi(strings.Replace(ss[len(ss)-1], ".ts", "", 1))

	if error != nil {
	}
	fakeTag := ""
	for index := 0; index < 1; index++ {
		no++
		noS := strconv.Itoa(no)
		fakeTag += infTag + "\n" + tsPrefix + noS + ".ts\n"
	}

	m3u8 += fakeTag
	return m3u8

}

func fixTargetDuration(m3u8 string) string {
	m3u8List := strings.Split(m3u8, "\n")
	maxDuration := 0.0
	tagDurationIdx := 0
	for i, tag := range m3u8List {
		if strings.Index(tag, "#EXT-X-TARGETDURATION") != -1 {
			tagDurationIdx = i
		}
		if strings.Index(tag, "#EXTINF") != -1 {
			tagEls := strings.Split(tag, ":")
			duration, _ := strconv.ParseFloat(strings.TrimRight(tagEls[1], ","), 64)
			if duration > maxDuration {
				maxDuration = duration
			}
		}
	}
	reg := regexp.MustCompile(`\d+`)
	m3u8List[tagDurationIdx] = reg.ReplaceAllString(m3u8List[tagDurationIdx], "${n}"+strconv.Itoa(int(math.Ceil(maxDuration))))
	return strings.Join(m3u8List, "\n")
}
